<?php

declare(strict_types=1);

namespace App\Http\Message;

use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Diactoros\Response\TextResponse;
use Zend\Diactoros\Response\XmlResponse;

final class ResponseFactory
{
    public function createEmpty(int $status = 200): ResponseInterface
    {
        return new EmptyResponse($status);
    }

    public function createHtml(string $html, int $status = 200): ResponseInterface
    {
        return new HtmlResponse($html, $status);
    }

    /**
     * @param mixed $data
     */
    public function createJson($data, int $status = 200): ResponseInterface
    {
        return new JsonResponse($data, $status);
    }

    public function createRedirect(string $uri): ResponseInterface
    {
        return new RedirectResponse($uri);
    }

    public function createText(string $text, int $status = 200): ResponseInterface
    {
        return new TextResponse($text, $status);
    }

    public function createXml(string $xml, int $status = 200): ResponseInterface
    {
        return new XmlResponse($xml, $status);
    }
}
