<?php

declare(strict_types=1);

namespace App\Page\Controller;

use App\Http\Message\ResponseFactory;
use App\Template\Renderer;
use Psr\Http\Message\ResponseInterface;

final class Index
{
    /** @var Renderer  */
    private $renderer;

    /** @var ResponseFactory */
    private $responseFactory;

    public function __construct(Renderer $renderer, ResponseFactory $responseFactory)
    {
        $this->renderer = $renderer;
        $this->responseFactory = $responseFactory;
    }

    public function __invoke(): ResponseInterface
    {
        return $this->responseFactory->createHtml(
            $this->renderer->render('page::index')
        );
    }
}
