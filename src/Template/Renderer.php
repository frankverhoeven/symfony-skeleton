<?php

declare(strict_types=1);

namespace App\Template;

interface Renderer
{
    /**
     * @param mixed[] $params
     */
    public function render(string $template, array $params = []): string;
}
