<?php

declare(strict_types=1);

namespace App\Template\Renderer;

use App\Template\Renderer;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

final class Twig implements Renderer
{
    /** @var Environment */
    private $environment;

    /** @var string */
    private $extension;

    public function __construct(Environment $environment, string $extension = '.html.twig')
    {
        $this->environment = $environment;
        $this->extension   = $extension;
    }

    /**
     * @param mixed[] $params
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(string $template, array $params = []): string
    {
        return $this->environment->render($this->normalizeTemplateName($template), $params);
    }

    private function normalizeTemplateName(string $template): string
    {
        $template = \str_replace('::', \DIRECTORY_SEPARATOR, $template);

        if (\strpos($template, $this->extension) === false) {
            return $template . $this->extension;
        }

        return $template;
    }
}
