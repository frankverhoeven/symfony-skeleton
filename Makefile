start:
	docker-compose up -d --build

stop:
	docker-compose stop

restart: stop start

git-pull:
	git pull

build:
	rm -rf data/cache/*
	docker-compose exec php composer install --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader
	docker-compose exec php vendor/bin/doctrine orm:clear-cache:metadata
	docker-compose exec php vendor/bin/doctrine orm:clear-cache:query
	docker-compose exec php vendor/bin/doctrine orm:clear-cache:result
	docker-compose exec php vendor/bin/doctrine-migrations migrations:migrate --no-interaction

analyse:
	docker-compose exec php vendor/bin/phpcs
	docker-compose exec php vendor/bin/psalm
	docker-compose exec php-xdebug vendor/bin/phpunit --coverage-text
