<?php

declare(strict_types=1);

namespace App\Tests\Template\Renderer;

use App\Template\Renderer\Twig;
use PHPUnit\Framework\TestCase;
use Twig\Environment;

final class TwigTest extends TestCase
{
    /** @var Environment */
    private $environment;

    /** @var Twig */
    private $renderer;

    protected function setUp(): void
    {
        $this->renderer = new Twig(
            $this->environment = $this->createMock(Environment::class)
        );
    }

    public function templateNameProvider(): \Generator
    {
        yield ['default', 'default.html.twig'];
        yield ['default.html.twig', 'default.html.twig'];
        yield ['default/home', 'default/home.html.twig'];
        yield ['default/home.html.twig', 'default/home.html.twig'];
        yield ['default::home', 'default/home.html.twig'];
        yield ['default::home.html.twig', 'default/home.html.twig'];
    }

    /**
     * @dataProvider templateNameProvider
     */
    public function testRendersWithNormalizedTemplateName(
        string $template,
        string $normalizedTemplate
    ): void {
        $this->environment->expects(self::once())
            ->method('render')
            ->with($normalizedTemplate, ['foo' => 'bar'])
            ->willReturn('html');

        /** @noinspection PhpUnhandledExceptionInspection */
        self::assertSame(
            'html',
            $this->renderer->render($template, ['foo' => 'bar'])
        );
    }
}
