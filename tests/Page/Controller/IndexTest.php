<?php

declare(strict_types=1);

namespace App\Tests\Page\Controller;

use App\Http\Message\ResponseFactory;
use App\Page\Controller\Index;
use App\Template\Renderer;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

final class IndexTest extends TestCase
{
    /** @var Index */
    private $controller;

    /** @var Renderer */
    private $renderer;

    /** @var ResponseFactory */
    private $responseFactory;

    protected function setUp(): void
    {
        $this->controller   = new Index(
            $this->renderer = $this->createMock(Renderer::class),
            $this->responseFactory = $this->createMock(ResponseFactory::class)
        );
    }

    public function testInvoke(): void
    {
        $this->renderer->expects(self::once())
            ->method('render')
            ->with('page::index')
            ->willReturn('html');

        $this->responseFactory->expects(self::once())
            ->method('createHtml')
            ->with('html')
            ->willReturn($response = $this->createMock(ResponseInterface::class));

        self::assertSame($response, ($this->controller)());
    }
}
