<?php

declare(strict_types=1);

namespace App\Tests\Http\Message;

use App\Http\Message\ResponseFactory;
use PHPUnit\Framework\TestCase;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Diactoros\Response\TextResponse;
use Zend\Diactoros\Response\XmlResponse;

final class ResponseFactoryTest extends TestCase
{
    /** @var ResponseFactory */
    private $factory;

    protected function setUp(): void
    {
        $this->factory = new ResponseFactory();
    }

    public function testCreateEmpty(): void
    {
        $response = $this->factory->createEmpty(418);

        self::assertInstanceOf(EmptyResponse::class, $response);
        self::assertSame(418, $response->getStatusCode());
    }

    public function testCreateXml(): void
    {
        $response = $this->factory->createXml('xml', 418);

        self::assertInstanceOf(XmlResponse::class, $response);
        self::assertSame('application/xml; charset=utf-8', $response->getHeader('content-type')[0]);
        self::assertSame('xml', (string) $response->getBody());
        self::assertSame(418, $response->getStatusCode());
    }

    public function testCreateText(): void
    {
        $response = $this->factory->createText('txt', 418);

        self::assertInstanceOf(TextResponse::class, $response);
        self::assertSame('text/plain; charset=utf-8', $response->getHeader('content-type')[0]);
        self::assertSame('txt', (string) $response->getBody());
        self::assertSame(418, $response->getStatusCode());
    }

    public function testCreateHtml(): void
    {
        $response = $this->factory->createHtml('html', 418);

        self::assertInstanceOf(HtmlResponse::class, $response);
        self::assertSame('text/html; charset=utf-8', $response->getHeader('content-type')[0]);
        self::assertSame('html', (string) $response->getBody());
        self::assertSame(418, $response->getStatusCode());
    }

    public function testCreateJson(): void
    {
        $response = $this->factory->createJson('json', 418);

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertSame('application/json', $response->getHeader('content-type')[0]);
        self::assertSame('"json"', (string) $response->getBody());
        self::assertSame(418, $response->getStatusCode());
    }

    public function testCreateRedirect(): void
    {
        $response = $this->factory->createRedirect('uri');

        self::assertInstanceOf(RedirectResponse::class, $response);
        self::assertSame('uri', $response->getHeader('location')[0]);
        self::assertSame(302, $response->getStatusCode());
    }
}
